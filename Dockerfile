FROM gitlab.topnet.rs:5050/openshift/image/webshop:python-slim
RUN mkdir -p /app
WORKDIR /app
COPY ./ ./

ARG ART_TARGET="develop"
ARG ART_HTTP_PROXY="http://10.253.130.5:8080"
ARG ART_HTTPS_PROXY="http://10.253.130.5:8080"
#
# Add the oracle instant client...
RUN . ./proxy.sh && apt-get update && apt-get install -y libaio1 curl gcc g++ unzip git telnet libmagic-dev libpq-dev && \
    cd /tmp && \
    curl -o instantclient-basiclite.zip https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip -SL && \
    unzip instantclient-basiclite.zip && \
    mv instantclient*/ /usr/lib/instantclient && \
    rm instantclient-basiclite.zip && \
    ln -s /usr/lib/instantclient/libclntsh.so.21.1 /usr/lib/libclntsh.so && \
    ln -s /usr/lib/instantclient/libocci.so.21.1 /usr/lib/libocci.so && \
    ln -s /usr/lib/instantclient/libociicus.so /usr/lib/libociicus.so && \
    ln -s /usr/lib/instantclient/libnnz21.so /usr/lib/libnnz21.so && \
    ln -s /usr/lib/libnsl.so.2 /usr/lib/libnsl.so.1 && \
    ln -s /lib/libc.so.6 /usr/lib/libresolv.so.2 && \
    ln -s /lib64/ld-linux-x86-64.so.2 /usr/lib/ld-linux-x86-64.so.2

# For A1
# ...
COPY config/core.example.yml config/core.yml
COPY config/external.example.yml config/external.yml
RUN . ./proxy.sh && python -m pip install --no-cache-dir -U pip virtualenv \
    && python -m venv .venv \
    && . .venv/bin/activate \
    && python -m pip install -U pip \
    && pip install -Ur requirements.txt
#    && cp ./hacks/any.py .venv/lib/python3.10/site-packages/zeep/xsd/types/any.py

# For MRVICA
#RUN python -m pip install --no-cache-dir -U pip virtualenv \
#    && python -m venv .venv \
#    && . .venv/bin/activate \
#    && python -m pip install -U pip \
#    && pip install -Ur requirements.txt

# Set proxy environment.
ENV ART_HTTP_PROXY=$ART_HTTP_PROXY
ENV ART_HTTPS_PROXY=$ART_HTTPS_PROXY

# Set OCI client path.
ENV LD_LIBRARY_PATH /usr/lib/instantclient

ENTRYPOINT ["/bin/bash"]
CMD ["./run.sh"]
