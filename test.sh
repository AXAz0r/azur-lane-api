#!/usr/bin/env bash

set -e

# Don't let CDPATH interfere with the cd command.
unset CDPATH
cd "$(dirname "$0")"

# Activate the python virtualenv.
source ".venv/bin/activate"

# Install dependencies.
pip install -U pytest flake8

# Run test suites.
python -m pytest -v tests/
python -m flake8 -v .
