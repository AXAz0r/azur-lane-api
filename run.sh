#!/usr/bin/env bash

set -e

# Don't let CDPATH interfere with the cd command.
unset CDPATH
cd "$(dirname "$0")"

# Activate the python virtualenv.
source ".venv/bin/activate"

# Execute the server.
exec python ./main.py

# Note: Make this gunicorn.
#exec gunicorn -w 12 -b 0.0.0.0:8000 main:APP
